﻿$(document).ready(function () {
    $('.stringTitle').click(function () {
        $('.string').slideToggle('slow');
        if ($('.ref').is(':visible')) {
            $('.ref').slideToggle('slow');
        }
        if ($('.ncbi').is(':visible')) {
            $('.ncbi').slideToggle('slow');
        }

    });
    $('.refTitle').click(function () {
        $('.ref').slideToggle('slow');
        if ($('.string').is(':visible')) {
            $('.string').slideToggle('slow');
        }
        if ($('.ncbi').is(':visible')) {
            $('.ncbi').slideToggle('slow');
        }
    });
    $('.ncbiTitle').click(function () {
        $('.ncbi').slideToggle('slow');
        if ($('.string').is(':visible')) {
            $('.string').slideToggle('slow');
        }
        if ($('.ref').is(':visible')) {
            $('.ref').slideToggle('slow');
        }
    });
});

//$(document).ready(function () {
//    $('.results').click(function () {
//        $('.showResults').slideToggle('fast');
//        if ($(this).text() == 'Show results') {
//            $(this).text('Hide results');
//        } else {
//            $(this).text('Show results');
//        }
//    });
//});

$(document).ready(function () {
    
    var MyRows = $('table#heat').find('tbody').find('tr');
    for (var i = 0; i < MyRows.length; i++) {
        for (var k = 1; k <= MyRows.length; k++) {
            var value = $(MyRows[i]).find('td:eq(' + k + ')').html();
            var b = 255 - (10 * value);
            var g = 255 - (10 * value);
            
            if (b < 0) {
                b = 0;
                g = 0;
            }           
            $(MyRows[i]).find('td:eq(' + k + ')').css('background-color', 'rgb(255,' + g + ', ' + b + ')');
        }
    }
});