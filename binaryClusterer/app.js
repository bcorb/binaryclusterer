﻿
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var http = require('http');
var path = require('path');

var app = express();

//create module objects
var hamming = require('./modules/hamming.js');
var cluster = require('./modules/clustering.js');
var formidable = require('formidable');
var db = require('./modules/dbConnection.js');
var getKmerList = require('./modules/kmer.js');

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(require('stylus').middleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
    app.use(express.errorHandler());
}

app.get('/', routes.index);
app.get('/about', routes.about);
app.get('/contact', routes.contact);
app.get('/hamming', routes.hamming);
app.get('/results', routes.results);
app.get('/heatMap', routes.heatMap);
app.get('/dbAccess', routes.dbAccess);
app.get('/scatter', routes.scatter);

//class variables
var data;
var fileNames;
var stored;
var sequenceNos;
var arr;
var seqs;

//start node server
http.createServer(app).listen(app.get('port'), function () {
    console.log('Express server listening on port ' + app.get('port'));
});

//clustering string bit
app.post('/hammingString', function (req, res) {
    ham(false, req, res);
});

//clustering reference list
app.post('/results', function (req, res) {
    ham(true, req, res);
});

//export as excel file
app.post('/exportExcel', function (req, res) {
    cluster.exportDataExcel(data);
    res.render('results', { save: 'File saved!', results: data });
});

//export as text file
app.post('/exportText', function (req, res) {
    cluster.exportDataText(data);
    res.render('results', { save: 'File saved!', results: data });
});

//generates heat map
app.post('/visual', function (req, res) {
    
    if (seqs != null) {
        var matrixData = cluster.visualise(data);
    } else {
        var matrixData = cluster.visualiseDB(data);
    }

    res.render('heatMap', { results: matrixData });
});

//generates scatter plot
app.post('/scatterPlot', function (req, res) {
    res.render('scatter', { results: JSON.stringify(data) });
});

//gets species
app.post('/species', function (req, res) {
    
    stored = req.body.btn != "Create own K-mers";

    db.displayOptions("SELECT SpeciesID FROM Species", function (species, err) {
        if (err) {
            console.log(err);
        } else {
            res.render('dbAccess', { species: species });
        }
    });
});

//gets genes
app.post('/getGenes', function (req, res) {
    var genes = [];
    var check = req.body;
    sequenceNos = Object.keys(check);
    sequenceNos.pop();
    
    for (var i = 0; i < sequenceNos.length; i++) {
        db.displayOptions("SELECT GeneKey, GenePID, GeneSynonym, GeneProduct FROM Genes WHERE SpeciesNo=" 
                            + sequenceNos[i], function (genes, err) {            
            if (err) {
                console.log(err);
            } else {
                res.render('dbAccess', { genes: genes, stored: stored });
            }
        });
    }
});

//calculates db scores
app.post('/dbResults', function (req, res) {

    var geneSeqs = [];
    var check = req.body;
    var kmerID = check.radio;
    var k = check.k;
    
    setArrays(check);    

    for (var i = 0; i < arr.length; i++) {
        db.displayOptions("SELECT GeneKey, Gene FROM GeneSeqs WHERE GeneKey=" + arr[i], function (genes, err) {
                       
            if (err) {
                console.log(err);
            } else {
                geneSeqs.push(genes[0])
            }

            if (geneSeqs.length == arr.length) {              
                if (!stored) {
                    own(k, kmerID, geneSeqs, res);
                } else {
                    storedKmers(geneSeqs, res);
                }
            }            
        });
    }                      
});

function setArrays(check) {
    
    arr = Object.keys(check);

    arr.pop();
    arr.pop();
    
    if (!stored) {
        arr.pop();
        arr.pop();
    }
    
    seqs = [];

    for (var key in check) {
        seqs.push(check[key]);
    }

    seqs.pop();
    seqs.pop();
};

storedKmers = function (geneSeqs, res) {
    db.displayOptions("SELECT CONVERT(bigint, MerMetadata) from Mer25", function (meta, err) {
        db.displayOptions("SELECT CONVERT(bigint, BasesPacked) from Mer25", function (bases, err) {
            
            var kmers = [];
            var md = [];
            var base = [];

            kmers.push(md);
            kmers.push(base);
            
            for (var i = 0; i < meta.length; i++) {
                for (var key in meta[i]) {
                    kmers[0].push(meta[i][key]);
                }
                
                for (var key in bases[i]) {
                    kmers[1].push(bases[i][key]);
                }
            }

            if (err) {
                    
            } else {
                hamming.dbHamming(geneSeqs, kmers, sequenceNos, stored, seqs, function (results, err) {
                    if (err) {

                    } else {
                        data = results;
                        res.render('results', { results: results });
                    }
                });
            } 
        });
    });
};

own = function (k, kmerID, geneSeqs, res) {

    db.displayOptions("SELECT GeneKey, Gene FROM GeneSeqs WHERE GeneKey=" 
                        + kmerID, function (kmer, err) {
        
        if (err) {
            console.log(err);
        } else {
            getKmerList.generateKmers(kmer[0], parseInt(k), function (kmers, err) {
                hamming.dbHamming(geneSeqs, kmers, sequenceNos, stored, seqs, function (results, err) {
                    data = results;
                    res.render('results', { results: results });
                });
            });
        }
    });
};

// hamming process
ham = function (refType, req, res) {
    console.time("total");
    var form = new formidable.IncomingForm();
    //validate files and get scores
    form.parse(req, function (err, fields, files) {
        fileNames = [];
        for (var file in files) {
            if (!hamming.validateFileExtension(files[file])) {
                res.render('hamming', { error: 'Invalid extension' });
            } else {
                fileNames.push(files[file].path);
            }
        }
             
        hamming.hammingScore(fileNames, refType, function (results, error) {    
            if (!error) {
                data = results;
                console.timeEnd("total");
                res.render('results', { results: results });
            } else {
                var error = results;
                res.render('hamming', { error: error });
            }
        });        
    });
};
