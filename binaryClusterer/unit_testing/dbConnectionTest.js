﻿var assert = require('assert');


exports['Correct conn string returns value'] = function () {
    
    var db = require('../modules/dbConnection.js');

    db.displayOptions("SELECT SpeciesID FROM Species", function (results, error) {
        assert.ok(results != null, 'Correct conn string returns value');
    });
}

exports['Incorrect conn string returns null value'] = function () {
    
    var db = require('../modules/dbConnection.js');
    
    db.displayOptions("SELECT Species FROM Species", function (results, error) {
        assert.ok(results == null, 'Incorrect conn string returns null value');
    });
}