﻿var assert = require('assert');
var cluster = require('../modules/clustering.js');
var hamming = require('../modules/hamming.js');

exports['Visual list returns value for kmer'] = function () {
    
    var hamming = require('../modules/hamming.js');
    var fileNames = [];
    
    fileNames.push("C:\\Users\\mac8\\Desktop\\reference list.txt");
    fileNames.push("C:\\Users\\mac8\\Desktop\\source file.txt");
    
    hamming.hammingScore(fileNames, true, function (results, error) {
        assert.ok(cluster.visualise(results) != null, 'Visual list returns value');
    });
}

exports['Visual list returns value for binary'] = function () {
    
    var hamming = require('../modules/hamming.js');
    var fileNames = [];
    
    fileNames.push("C:\\Users\\mac8\\Desktop\\file1.txt");
    
    hamming.hammingScore(fileNames, false, function (results, error) {
        assert.ok(cluster.visualise(results) != null, 'Visual list returns value');
    });
}

exports['Visual list returns correct value for kmer'] = function () {
    
    var hamming = require('../modules/hamming.js');
    var fileNames = [];
    
    fileNames.push("C:\\Users\\mac8\\Desktop\\reference list.txt");
    fileNames.push("C:\\Users\\mac8\\Desktop\\source file.txt");
    
    hamming.hammingScore(fileNames, true, function (results, error) {
        assert.ok(cluster.visualise(results) != null, 'Visual list returns value');
    });
}

exports['Visual list returns correct value for binary'] = function () {
    
    var hamming = require('../modules/hamming.js');
    var fileNames = [];
    
    fileNames.push("C:\\Users\\mac8\\Desktop\\file1.txt");
    
    hamming.hammingScore(fileNames, false, function (results, error) {
        assert.ok(cluster.visualise(results) != null, 'Visual list returns value');
    });
}