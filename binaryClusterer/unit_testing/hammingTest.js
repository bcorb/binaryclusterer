﻿var assert = require('assert');

exports['K-mer comparison returns value'] = function () {
    
    var hamming = require('../modules/hamming.js');
    var fileNames = [];
    
    fileNames.push("C:\\Users\\mac8\\Desktop\\reference list.txt");
    fileNames.push("C:\\Users\\mac8\\Desktop\\source file.txt");
    
    hamming.hammingScore(fileNames, true, function (results, error) {
        assert.ok(results != null, 'K-mer comparison returns value');
    });
}

exports['Binary comparison returns value'] = function () {
    
    var hamming = require('../modules/hamming.js');
    var fileNames = [];
    
    fileNames.push("C:\\Users\\mac8\\Desktop\\file1.txt");
    
    hamming.hammingScore(fileNames, false, function (results, error) {
        assert.ok(results != null, 'Binary comparison returns value');
    });
}

exports['K-mer comparison returns correct value'] = function () {
    
    var hamming = require('../modules/hamming.js');
    var fileNames = [];
    
    fileNames.push("C:\\Users\\mac8\\Desktop\\reference list.txt");
    fileNames.push("C:\\Users\\mac8\\Desktop\\source file.txt");
    
    var results = hamming.hammingScore(fileNames, true, function (results, error) {
        assert.ok(results[0].hammingDistance == 0, 'K-mer comparison returns correct value');
        assert.ok(results[1].hammingDistance == 3, 'K-mer comparison returns correct value');
    });
}

exports['K-mer comparison returns correct array length'] = function () {
    
    var hamming = require('../modules/hamming.js');
    var fileNames = [];

    fileNames.push("C:\\Users\\mac8\\Desktop\\source file.txt");
    fileNames.push("C:\\Users\\mac8\\Desktop\\reference list.txt");
    
    var results = hamming.hammingScore(fileNames, true, function (results, error) {
        assert.ok(results.length == 196, 'K-mer comparison returns correct value');
    });
}

exports['Binary comparison returns correct array length'] = function () {
    
    var hamming = require('../modules/hamming.js');
    var fileNames = [];
    
    fileNames.push("C:\\Users\\mac8\\Desktop\\file1.txt");
    
    var results = hamming.hammingScore(fileNames, true, function (results, error) {
        assert.ok(results.length == 16, 'Binary comparison returns correct value');
    });
}


exports['K-mer comparison returns correct sequence values'] = function () {
    
    var hamming = require('../modules/hamming.js');
    var fileNames = [];
    
    fileNames.push("C:\\Users\\mac8\\Desktop\\source file.txt");
    fileNames.push("C:\\Users\\mac8\\Desktop\\reference list.txt");
    
    var results = hamming.hammingScore(fileNames, true, function (results, error) {
        assert.ok(results[0].firstSeq == "CTGGAATGATCC", 'K-mer comparison returns correct sequence values');
        assert.ok(results[0].secondSeq == "CTGGAATGATCC", 'K-mer comparison returns correct sequence values');
    });
}

exports['K-mer comparison returns correct binary values'] = function () {
    
    var hamming = require('../modules/hamming.js');
    var fileNames = [];
    
    fileNames.push("C:\\Users\\mac8\\Desktop\\source file.txt");
    fileNames.push("C:\\Users\\mac8\\Desktop\\reference list.txt");
    
    var results = hamming.hammingScore(fileNames, true, function (results, error) {
        assert.ok(results[0].firstBit == "10010101100011", 'K-mer comparison returns correct binary values');
        assert.ok(results[0].secondBit == "10010101100011", 'K-mer comparison returns correct binary values');
    });
}

exports['K-mer comparison returns correct hamming'] = function () {
    
    var hamming = require('../modules/hamming.js');
    var fileNames = [];

    fileNames.push("C:\\Users\\mac8\\Desktop\\source file.txt");
    fileNames.push("C:\\Users\\mac8\\Desktop\\reference list.txt");
    
    var results = hamming.hammingScore(fileNames, true, function (results, error) {
        assert.ok(results[0].hammingDistance == 0, 'K-mer comparison returns correct value');
        assert.ok(results[1].hammingDistance == 3, 'K-mer comparison returns correct value');
    });
}

exports['Binary comparison returns correct value'] = function () {
    
    var hamming = require('../modules/hamming.js');
    var fileNames = [];
    
    fileNames.push("C:\\Users\\mac8\\Desktop\\file1.txt");
    
    var results = hamming.hammingScore(fileNames, false, function (results, error) {
        assert.ok(results[0].hammingDistance == 0, 'Binary comparison returns correct value');
        assert.ok(results[1].hammingDistance == 7, 'Binary comparison returns correct value');
    });
}

exports['K-mer comparison returns correct value'] = function () {
    
    var hamming = require('../modules/hamming.js');
    var fileNames = [];

    fileNames.push("C:\\Users\\mac8\\Desktop\\source file.txt");
    fileNames.push("C:\\Users\\mac8\\Desktop\\reference list.txt");
    
    var results = hamming.hammingScore(fileNames, true, function (results, error) {
        assert.ok(results.length == 196, 'K-mer comparison returns correct value');
    });
}

exports['Binary comparison returns correct seq values'] = function () {
    
    var hamming = require('../modules/hamming.js');
    var fileNames = [];
    
    fileNames.push("C:\\Users\\mac8\\Desktop\\file1.txt");
    
    var results = hamming.hammingScore(fileNames, false, function (results, error) {
        assert.ok(results[0].firstSeq == "101110001001", 'Binary comparison returns correct seq values');
        assert.ok(results[0].secondSeq == "101110001001", 'Binary comparison returns correct seq values');
    });
}

exports['Binary comparison returns correct binary values'] = function () {
    
    var hamming = require('../modules/hamming.js');
    var fileNames = [];
    
    fileNames.push("C:\\Users\\mac8\\Desktop\\file1.txt");
    
    var results = hamming.hammingScore(fileNames, false, function (results, error) {
        assert.ok(results[0].firstBit == "101110001001", 'Binary comparison returns correct binary values');
        assert.ok(results[0].secondBit == "101110001001", 'Binary comparison returns correct binary values');
    });
}

exports['Binary comparison different bit lengths exception'] = function () {
    
    var hamming = require('../modules/hamming.js');
    var fileNames = [];
    
    fileNames.push("C:\\Users\\mac8\\Desktop\\error.txt");
    
    var results = hamming.hammingScore(fileNames, false, function (results, error) {
        assert.ok(results[1].hammingDistance == "bit length not valid", 'Binary comparison different bit lengths exception');
    });
}

exports['Incorrect file type k-mer exception'] = function () {
    
    var hamming = require('../modules/hamming.js');
    var fileNames = [];
    
    fileNames.push("C:\\Users\\mac8\\Desktop\\notvalid.docx");
    
    var results = hamming.hammingScore(fileNames, false, function (results, error) {
        assert.ok(results == "File not valid", 'Incorrect file type k-mer exception');
    });
}

exports['Incorrect file type binary exception'] = function () {
    
    var hamming = require('../modules/hamming.js');
    var fileNames = [];
    
    fileNames.push("C:\\Users\\mac8\\Desktop\\notvalid.docx");
    
    var results = hamming.hammingScore(fileNames, false, function (results, error) {
        assert.ok(results == "File not valid", 'Incorrect file type binary exception');
    });
}