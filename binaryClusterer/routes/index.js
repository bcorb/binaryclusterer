﻿
/*
 * GET home page.
 */

exports.index = function (req, res) {
    res.render('index', { title: 'Express', year: new Date().getFullYear() });
};

exports.about = function (req, res) {
    res.render('about', { title: 'About', year: new Date().getFullYear(), message: 'Your application description page.' });
};

exports.contact = function (req, res) {
    res.render('contact', { title: 'Contact', year: new Date().getFullYear(), message: 'Your contact page.' });
};

exports.hamming = function (req, res) {
    res.render('hamming', { title: 'Hamming', year: new Date().getFullYear(), message: 'Hamming and clustering calculation' });
};

exports.results = function (req, res) {
    res.render('results', { title: 'Results', year: new Date().getFullYear(), message: 'Binary clustering results' });
};

exports.heatMap = function (req, res) {
    res.render('heatMap', { title: 'heatMap', year: new Date().getFullYear(), message: 'Heat Map of results' });
};

exports.dbAccess = function (req, res) {
    res.render('dbAccess', { title: 'Data', year: new Date().getFullYear(), message: 'Access sequence data' });
};

exports.scatter = function (req, res) {
    res.render('scatter', { title: 'Data', year: new Date().getFullYear(), message: 'Generates scatter plot' });
};