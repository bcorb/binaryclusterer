﻿var kmers = [];

exports.generateKmers = function (sequence, k, callback) {
    
    for (var i = 0; i < sequence.Gene.length - k + 1; i++) {
        getKmer(sequence, i, k, function (err) {
            if (err) {
                console.log(err);
            } else {
                if (kmers.length == sequence.Gene.length - k + 1) {
                    callback(kmers);
                }
            }
        });
    }    
};

function getKmer(kmer, i, k, callback) {
    
    kmers.push(kmer.Gene.substring(i, i + k));
    callback();
};