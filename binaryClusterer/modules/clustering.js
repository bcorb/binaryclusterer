﻿var csvWriter = require('csv-write-stream');
var fs = require('fs');

var TEXT_FILE_NAME = 'TextResuls.txt';
var CSV_FILE_NAME = 'CsvResults.csv';
var SEQ1_COL = "Sequence 1";
var BIT1_COL = "Bit Value 1";
var SEQ2_COL = "Sequence 2";
var BIT2_COL = "Bit Value 2";
var HAM_COL = "Hamming Score";

// export of data to excel
exports.exportDataExcel = function (results) {
    var writer = csvWriter({
        headers: [SEQ1_COL, BIT1_COL, SEQ2_COL, BIT2_COL, HAM_COL]
    })
    writer.pipe(fs.createWriteStream(CSV_FILE_NAME));
    
    for (var i = 0; i < results.length; i++) {
        writer.write([results[i].firstSeq.toString(), 
                     results[i].firstBit.toString(), 
                     results[i].secondSeq.toString(),
                     results[i].secondBit.toString(), 
                     results[i].hammingDistance.toString()])
    }    
    writer.end();
};

// export of data to text
exports.exportDataText = function (results) {
    var wstream = fs.createWriteStream(TEXT_FILE_NAME);  
    for (var i = 0; i < results.length; i++) {
        wstream.write(results[i].firstSeq.toString() + " " +
                 results[i].firstBit.toString() + " " +
                 results[i].secondSeq.toString() + " " +
                 results[i].secondBit.toString() + " " + 
                 results[i].hammingDistance.toString() + "\n");
    }
};

// creates the heat map to be dispalyed in browser
exports.visualise = function (results) {
    
    var visualList = [];
    
    // creates array item for each line in matrix
    for (var i = 0; i < results[0].numberSequences; i++) {
        var line = [];
        line.push(results[i].secondSeq);
        visualList.push(line);
    }
    
    // adds in results objects to appropriate line in matrix
    for (var k = 0; k < results.length; k++) {      
        visualList[results[k].id1].push(results[k].hammingDistance);
    }

    return visualList;
};

// creates the heat map to be dispalyed in browser
exports.visualiseDB = function (results, seqs) {
    
    var visualList = [];
    
    // creates array item for each line in matrix
    for (var i = 0; i < results[0].numberSequences; i++) {
        var line = [];
        line.push(results[i].secondSeq);
        visualList.push(line);
    }
    
    // adds in results objects to appropriate line in matrix
    for (var k = 0; k < results.length; k++) {
        visualList[results[k].id1].push(results[k].hammingDistance);
    }
    
    return visualList;
};
