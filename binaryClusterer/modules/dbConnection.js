﻿var edge = require('edge');

var SQL_CONNECTION_STRING = "Data Source=localhost;Initial Catalog=NCBIBactSubset;Integrated Security=True";

exports.displayOptions = function (sqlQuery, callback) {
    dbConnection(sqlQuery, function (results, err) {
        if (err) {
            console.log(err);
        } else {
            callback(results);
        }
    });
};

function dbConnection(sqlQuery, callback) {   
    var params = {
        connectionString: SQL_CONNECTION_STRING,
        source: sqlQuery
    };
    
    var callDB = edge.func('sql', params);
    
    callDB(null, function (err, result) {
        if (err) {
            console.log(err);
        } else {
            callback(result);
        }
    });
};