﻿var fs = require('fs');
var hamming = require('compute-hamming');
var path = require('path');

var bitArray;
var hammingDistance;
var dataArray = [];
var bits;
var file1;
var file2;
var refList = [];
var comparisonList;
var results;
var done1 = false;
var done2 = false;
var ref = false;
var uniqueCount = 0;
var species = [];
var dbData = false;

var NOT_VALID = "File not valid";
var TEXT_FILE = '.txt';
var BIN_FILE = '.bin';
var POSITIVE_BIT = '1';
var NEGATIVE_BIT = '0';
var NOT_FOUND = -1;
var BIT_LENGTH_ERROR = "bit length not valid"

// performs all actions for the hamming calculation
exports.hammingScore = function (fileNames, refType, callback) {

    ref = refType;
    clearArrays();
    addFiles(refType);

    if (!validateFiles(fileNames)) {
        callback(NOT_VALID, true)
    } else {      
        for (var i = 0; i < fileNames.length; i++) {
            readFile(fileNames[i], i, function (error) {
                if (refType) {
                    convertReferenceList(function (err) {
                        refScore(function (err) {
                            callback(results, false);
                        });
                    });                    
                } else {
                    score(function (err) {
                        callback(results, false);
                    });
                }
            });
        }
    }
};

//sets all arrays to zero
function clearArrays() {
    bits = [];
    file1 = [];
    file2 = []
    comparisonList = [];
};

//adds file data to array used in calculation
function addFiles(refType) {
    if (refType) {
        bits.push(file1);
        bits.push(file2);
    } else {
        bits.push(file1);
    }
};

//calculates hamming scores for seqeunces from db
exports.dbHamming = function (genes, kmers, sequenceNos, stored, speciesList, callback) {

    clearArrays();
    species = speciesList;
    dbData = true;

    if (stored) {
        var kmerValues = getKmers(kmers, sequenceNos);
        kmers = kmerValues;
    }

    for (var i = 0; i < genes.length; i++) {
        file1.push(genes[i].Gene);
    }
    
    for (var i = 0; i < kmers.length; i++) {
        file2.push(kmers[i])
    }
    file2.push(kmers);
    
    convertReferenceList(function (err) {
        refScore(function (err) {
            callback(results, false);
        });
    });
};

//extracts the kmer binary values for selected inputted species numbers
function getKmers(kmers, sequenceNos) {
    
    var kmerValues = [];
    
    for (var i = 0; i < kmers[0].length; i++) {
        for (var k = 0; k < sequenceNos.length; k++) {
            console.log(kmers[0][i].toString()[0]);
            if (Number(kmers[0][i].toString()[0]) == sequenceNos[k]) {
                kmerValues.push(kmers[1][i]);
            }
        }
    }
    return converyBinaryKmers(kmerValues);
};

//get ACTG values from binary
function converyBinaryKmers(kmerValues) {

    var converted = [];
    
    for (var i = 0; i < kmerValues.length; i++) {
        var kmer = '';
        for (var k = 0; k < kmerValues[i].toString().length; k++) {
            if (Number(kmerValues[i].toString()[k]) == 0) {
                kmer += 'A';
            } else if (Number(kmerValues[i].toString()[k]) == 1) {
                kmer += 'C';
            } else if (Number(kmerValues[i].toString()[k]) == 10) {
                kmer += 'G';
            } else if (Number(kmerValues[i].toString()[k]) == 11) {
                kmer += 'T';
            }
        }
        converted.push(kmer);
    }

    return converted;
};

// checks that files exist in directory
function validateFiles(fileNames) {

    for (var i = 0; i < fileNames.length; i++) {
        if (!fs.existsSync(fileNames[i])) {
            return false;
        }
    }
    return true;
};

// checks that the files are correct format
exports.validateFileExtension = function (fileName) {
    if (path.extname(fileName.name) != TEXT_FILE && path.extname(fileName.name) != BIN_FILE) {
        return false;
    } else {
        return true;
    }
};

// validates that the two input bits are the same length
function validateBitLength(bit1, bit2) {
    return bit1.length != bit2.length;
};

// convert the refernce list to bit values
function convertReferenceList(callback) {
    for (var i = 0; i < file1.length; i++) {
        var binResult = '';

        for (var k = 0; k < file2.length; k++) {
            if (file1[i].indexOf(file2[k]) > NOT_FOUND) {
                binResult += POSITIVE_BIT
            } else {
                binResult += NEGATIVE_BIT
            }
        }
        comparisonList.push(binResult);

        if (i == file1.length - 1) {
            callback();
        }
    }
};

// reads a single file
function readFile(fileName, fileNumber, callback) {
    
    var stringLength;
    var LineByLineReader = require('line-by-line'),
        lr = new LineByLineReader(fileName);
        
    lr.on('error', function (err) {
        console.log(err);
    });
        
    // each line
    lr.on('line', function (line) {
        bits[fileNumber].push(line);
    });
        
    // end of file
    lr.on('end', function () {      
        if (fileNumber == 0) {
            done1 = true;
        } else {
            done2 = true;
        }
               
        if (done1 && done2 || !ref) {
            callback(false);
        }
    });
};

// calculates hamming distance for the binary comparison
function score(callback) {
    
    var added = 0;
    results = [];

    // hamming distance calculation for each bitset in Vector array
    for (var i = 0; i < file1.length; i++) {
        for (var k = 0; k < file1.length; k++) {
                       
            addScore(i, k, function (err) {
                if (err) {
                    callback(err);
                }
                //exits when all results have been added               
                if (++added == file1.length * 2) {
                    callback();
                }
            });
        }
    }
};

// adds values to results data structure for k-mer values
function addScore(i, k, callback) {

    var thisResult = new Result();
    
    thisResult.uniqueID = uniqueCount;
    thisResult.numberSequences = file1.length;
    
    thisResult.firstSeq = file1[i];
    thisResult.secondSeq = file1[k];
    thisResult.firstBit = file1[i];
    thisResult.secondBit = file1[k];
    thisResult.id1 = i;
    thisResult.id2 = k;
    thisResult.hammingDistance = hammingDistance(file1[i], file1[k]);
    
    results.push(thisResult);
    
    uniqueCount++;
    callback();
};

// adds values to results data structure for binary values
function addScoreRef(i, k, callback) {

    var thisResult = new Result();
    
    thisResult.uniqueID = uniqueCount;    
    thisResult.numberSequences = comparisonList.length;
    
    if (!dbData) {
        thisResult.firstSeq = file1[i];
        thisResult.secondSeq = file1[k];
    } else {
        thisResult.firstSeq = "GenePID - " + species[i];
        thisResult.secondSeq = "GenePID - " + species[k];
    }

    thisResult.firstBit = comparisonList[i];
    thisResult.secondBit = comparisonList[k];
    thisResult.id1 = i;
    thisResult.id2 = k;
    thisResult.hammingDistance = hammingDistance(comparisonList[i], comparisonList[k]);
    
    results.push(thisResult);
    
    uniqueCount++;
    callback();
};

// calculates the hamming distance for the reference list comparison
function refScore(callback) {
    
    var added = 0;
    results = [];

    for (var i = 0; i < comparisonList.length; i++) {
        for (var k = 0; k < comparisonList.length; k++) {
            addScoreRef(i, k, function (err) {
                if (err) {
                    callback(err);
                    return;
                }
                //exits when all results have been added               
                if (++added == file1.length * file1.length) {
                    callback();
                }
            });
        }
    }
};

// hamming distance calcuation
hammingDistance = function (b1, b2) {    
    try {
        return hamming(b1, b2);
    } catch (err) {
        return BIT_LENGTH_ERROR;
    }
};

// Data structure to store binary results
function Result() {
    this.firstBit = "0";
    this.secondBit = "0";
    this.firstSeq = "0";
    this.secondSeq = "0";
    this.hammingDistance = 0;
    this.id1 = 0;
    this.id2 = 0;
    this.number = 0;
    this.numberSequences = 0;
    this.uniqueID = 0;
};